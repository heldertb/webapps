var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var UserSchema = new mongoose.Schema({
    firstname: String,
    lastname: String,
    username: {
        type: String,
        unique: true
    },
    hash: String,
    salt: String,
    email: {
        type: String,
        unique: true
    },
    gender: String,
    karma: Number
});

UserSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');

    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

UserSchema.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');

    return this.hash === hash;
};

UserSchema.methods.generateJWT = function() {

    // set expiration to 5 days
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(today.getDate() + 5);

    return jwt.sign({
        _id: this._id,
        userName: this.userName,
        exp: parseInt(exp.getTime() / 1000),
    }, 'SECRET');
};

var User = mongoose.model('User', UserSchema);
module.exports = User;