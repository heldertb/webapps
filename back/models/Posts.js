var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
	author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    authorname: String,
    title: String,
    body: String,
    link: String,
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment'
    }],
    votedby: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    votes: Number,
    islink: Boolean
});

var Post = mongoose.model('Post', PostSchema);
module.exports = Post;