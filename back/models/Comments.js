var mongoose = require('mongoose');

var CommentSchema = new mongoose.Schema({
	author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    authorname: String,
    votedby: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    comment: String,
    votes: Number
});

var Comment = mongoose.model('Comment', CommentSchema);
module.exports = Comment;