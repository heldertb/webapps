var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');

var comments = {
    upvote: function(req, res, next) {
        if (!req.user || !req.comment) {
            return next(new Error('Please make sure you are logged in'));
        }
        if (req.comment.votedby.indexOf(req.user._id) > -1 || req.comment.author === req.user._id) {
            return;
        }
        req.comment.votes = req.comment.votes + 1;
        req.comment.votedby.push(req.user._id);

        var query = User.findById(req.comment.author);

        query.exec(function(err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next(new Error('Could not find user'));
            }
            user.karma = req.user.karma + 1;
            user.save(function(err) {
                if (err)
                    return next(new Error('Ooops! Something went wrong...'));
            });
        });

        req.comment.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else
                return res.json(req.comment);
        });
    },

    downvote: function(req, res, next) {
        if (!req.user || !req.comment) {
            return next(new Error('Please make sure you are logged in'));
        }
        if (req.comment.votedby.indexOf(req.user._id) > -1 || req.comment.author === req.user._id) {
            return;
        }
        req.comment.votes = req.comment.votes - 1;
        req.comment.votedby.push(req.user._id);
        var query = User.findById(req.comment.author);

        query.exec(function(err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next(new Error('Could not find user'));
            }
            user.karma = req.user.karma - 1;
            user.save(function(err) {
                if (err)
                    return next(new Error('Ooops! Something went wrong...'));
            });
        });
        req.comment.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else
                return res.json(req.comment);
        });
    },
};

module.exports = comments;