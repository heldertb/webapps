var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');
var users = require('./users.js');
var posts = require('./posts.js');
var comments = require('./comments.js');
var jwt = require('express-jwt');
var auth = jwt({
    secret: 'SECRET',
    userProperty: 'payload'
});

/* GET phome page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* POST users */
router.post('/login', users.login);
router.post('/register', users.register);
/* GET users */
router.get('/getpersonaldata/:user', auth, users.getPersonalData);
router.get('/getusername/:user', auth, users.getUserName);

/* POST posts */
router.post('/posts/add/:user', auth, posts.addPost);
router.post('/posts/addcomment/:user/:post', auth, posts.addComment);
/* PUT posts */
router.put('/posts/upvote/:user/:post', auth, posts.upvote);
router.put('/posts/downvote/:user/:post', auth, posts.downvote);
/* GET posts */
router.get('/posts/getall/:user', auth, posts.getAll);
router.get('/posts/getone/:post', auth, posts.getOne);
router.get('/posts/getcomments/:post', auth, posts.getComments);

/* PUT comments */
router.put('/comments/upvote/:user/:comment', auth, comments.upvote);
router.put('/comments/downvote/:user/:comment', auth, comments.downvote);

router.param('user', function(req, res, next, id) {
    var query = User.findById(id);

    query.exec(function(err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next(new Error('Could not find user'));
        }

        req.user = user;
        return next();
    });
});

router.param('post', function(req, res, next, id) {
    var query = Post.findById(id);

    query.exec(function(err, post) {
        if (err) {
            return next(err);
        }
        if (!post) {
            return next(new Error('Could not find post'));
        }

        req.post = post;
        return next();
    });
});

router.param('comment', function(req, res, next, id) {
    var query = Comment.findById(id);

    query.exec(function(err, comment) {
        if (err) {
            return next(err);
        }
        if (!comment) {
            return next(new Error('Could not find comment'));
        }

        req.comment = comment;
        return next();
    });
});

module.exports = router;
