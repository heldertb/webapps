var mongoose = require('mongoose');
var passport = require('passport');
var User = mongoose.model('User');
var schedule = require('node-schedule');

var users = {
	login: function(req, res, next) {
        if (!req.body.username || !req.body.password) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }

        passport.authenticate('local', function(err, user, info) {
            if (err) {
                return next(err);
            }

            if (user) {
                return res.json({
                    id: user._id,
                    username: user.username,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    karma: user.karma,
                    token: user.generateJWT()
                });
            } else {
                return res.status(401).json(info);
            }
        })(req, res, next);
    },

    getUserName: function(req, res, next) {
        return res.json(req.user);
    },

    register: function(req, res, next) {
        if (!req.body.password || !req.body.email || !req.body.firstname || !req.body.lastname) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }

        var user = new User();
        if (!req.body.username) {
            user.username = req.body.email;
        } else {
            user.username = req.body.username;
        }

        user.setPassword(req.body.password)

        user.firstname = req.body.firstname;
        user.lastname = req.body.lastname;
        user.email = req.body.email;
        user.gender = req.body.gender;
        user.karma = 0;

        user.save(function(err) {
            if (err) {
                return next(err);
            }

            return res.json({
                id: user._id,
                username: user.username,
                firstname: user.firstname,
                lastname: user.lastname,
                karma: user.karma,
                token: user.generateJWT()
            });
        });
    },

    getPersonalData: function(req, res, next) {
    	if(!req.user) {
    		return next(new Error('Please make sure you are logged in'));
    	}
    	return res.json({
    		username: req.user.username,
    		firstname: req.user.firstname,
    		lastname: req.user.lastname,
    		email: res.user.email,
    		gender: req.user.gender
    	});
    }
};

module.exports = users;