var mongoose = require('mongoose');
var User = mongoose.model('User');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');

var posts = {
    getAll: function(req, res, next) {
        Post.find(function(err, posts) {
            if (err) {
                return next(err);
            }
            res.json(posts);
        });
    },

    getOne: function(req, res, next) {
        return res.json(req.post);
    },

    addPost: function(req, res, next) {
        if (!req.user) {
            return next(new Error('Could not find user'));
        }
        if (!req.body.title) {
            return res.status(400).json({
                message: 'Please fill out all fields'
            });
        }
        var post = new Post();
        post.title = req.body.title;
        if (req.body.body) {
            post.body = req.body.body;
            post.islink = false;
        } else {
            post.link = req.body.link;
            post.islink = true;
        }
        post.author = req.user._id;
        post.authorname = req.user.username;
        post.votes = 1;
        post.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else
                return res.json(post);
        });
    },

    upvote: function(req, res, next) {
        if (!req.user || !req.post) {
            return next(new Error('Please make sure you are logged in'));
        }
        if (req.post.votedby.indexOf(req.user._id) > -1 || req.post.author === req.user._id) {
            return;
        }
        req.post.votes = req.post.votes + 1;
        req.post.votedby.push(req.user._id);
        var query = User.findById(req.post.author);

        query.exec(function(err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next(new Error('Could not find user'));
            }
            user.karma = req.user.karma + 1;
            user.save(function(err) {
                if (err)
                    return next(new Error('Ooops! Something went wrong...'));
            });
        });
        req.post.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else
                return res.json(req.post);
        });
    },

    downvote: function(req, res, next) {
        if (!req.user || !req.post) {
            return next(new Error('Please make sure you are logged in'));
        }
        if (req.post.votedby.indexOf(req.user._id) > -1) {
            return;
        }
        req.post.votes = req.post.votes - 1;
        req.post.votedby.push(req.user._id);
        var query = User.findById(req.post.author);

        query.exec(function(err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next(new Error('Could not find user'));
            }
            user.karma = req.user.karma + 1;
            user.save(function(err) {
                if (err)
                    return next(new Error('Ooops! Something went wrong...'));
            });
        });
        req.post.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else
                return res.json(req.post);
        });
    },

    addComment: function(req, res, next) {
        if (!req.user || !req.post) {
            return next(new Error('Please make sure you are logged in'));
        }
        var comment = new Comment();
        comment.author = req.user._id;
        comment.authorname = req.user.username;
        comment.comment = req.body.body;
        comment.votes = 1;

        comment.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
        });

        req.post.comments.push(comment._id);
        req.post.save(function(err) {
            if (err)
                return next(new Error('Ooops! Something went wrong...'));
            else
                return res.json(comment);
        });
    },

    getComments: function(req, res, next) {
        Comment.find({
                _id: {
                    $in: req.post.comments
                }
            },
            function(err, results) {
                return res.json(results);
            });
    }
};

module.exports = posts;