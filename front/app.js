var app = angular.module('flapperNews', ['ui.router']);

app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('login', {
                url: '/',
                templateUrl: 'views/login.html',
                controller: 'MainCtrl'
            }).state('home', {
                resolve: {
                    "check": function($location, $rootScope) {
                        if (!$rootScope.loggedIn) {
                            $location.path('/');
                        }
                    }
                },
                url: '/home',
                templateUrl: 'views/home.html',
                controller: 'PostsCtrl'
            }).state('posts', {
                resolve: {
                    "check": function($location, $rootScope) {
                        if (!$rootScope.loggedIn) {
                            $location.path('/');
                        }
                    }
                },
                url: '/posts/{id}',
                templateUrl: 'views/posts.html',
                controller: 'SinglePostCtrl'
            })
            .state('add', {
                resolve: {
                    "check": function($location, $rootScope) {
                        if (!$rootScope.loggedIn) {
                            $location.path('/');
                        }
                    }
                },
                url: '/addpost',
                templateUrl: 'views/addpost.html',
                controller: 'AddPostCtrl'
            }).state('register', {
                url: '/register',
                templateUrl: 'views/register.html',
                controller: 'MainCtrl'
            });

        $urlRouterProvider.otherwise('home');
    }
]);

app.controller('MainCtrl', function($scope, $location, $rootScope, $http, auth) {

    $scope.login = function() {
        if ($scope.username !== null && $scope.password !== null) {

            $http({
                method: 'POST',
                url: 'http://localhost:3000/login',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json'
                },
                data: {
                    username: $scope.username,
                    password: $scope.password
                }
            }).then(function successCallback(response) {
                $rootScope.loggedIn = true;
                $rootScope.username = $scope.username;
                $rootScope.userid = response.data.id;
                auth.saveToken(response.data.token);
                $location.path('/home');
            }, function errorCallback(response) {
                // show error message
                if (response.data !== null) {
                    $scope.error = response.data.message;
                } else {
                    $scope.error = response.data.message;
                    $location.path('/ise');
                }
            });
        } else {
            $scope.error = "Gelieve alle gegevens in te vullen.";
        }
    };

    $scope.registerredirect = function() {
        $location.path('/register');
    };

    $scope.postRedirect = function() {
        $location.path('/addpost');
    }

    $scope.register = function() {
        $rootScope.username = '';
        if ($scope.password === $scope.confirmpassword) {
            if ($scope.email !== null && $scope.password !== null && $scope.confirmpassword !== null && $scope.firstName !== null && $scope.lastName !== null &&
                $scope.email !== '' && $scope.password !== '' && $scope.confirmpassword !== '' && $scope.firstName !== '' && $scope.lastName !== '') {
                if ($scope.username === '' || $scope.username === null) {
                    $scope.username = $scope.email;
                }
                $http({
                    method: 'POST',
                    url: 'http://localhost:3000/register',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                    data: {
                        username: $scope.username,
                        password: $scope.password,
                        firstname: $scope.firstName,
                        lastname: $scope.lastName,
                        email: $scope.email,
                        gender: $scope.gender
                    }
                }).then(function succesCallBack(response) {
                    $rootScope.loggedIn = true;
                    $rootScope.username = $scope.username;
                    $rootScope.userid = response.data.id;
                    auth.saveToken(response.data.token);
                    $location.path('/home');
                }, function errorCallback(response) {
                    console.log(response);
                    if (response.data !== null) {
                        $scope.error = response.data.message;
                    } else {
                        $location.path('/ise');
                    }
                });
            } else {
                $scope.error = "Please fill in all required fields.";
            }
        } else {
            $scope.error = "Password and confirm password have to be te same.";
        }
    };
});

app.controller('AddPostCtrl',
    function($scope, $location, $rootScope, $http, auth) {
        $scope.addPost = function() {
            if (!$scope.title || $scope.title === '') {
                return;
            }
            $http({
                method: 'POST',
                url: 'http://localhost:3000/posts/add/' + $rootScope.userid,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                },
                data: {
                    title: $scope.title,
                    body: $scope.body,
                    link: $scope.link
                }
            }).then(function successCallback(response) {
                $location.path('/home');
            }, function errorCallback(response) {
                console.log(response);
                // show error message
                if (response.data !== null) {
                    $scope.error = response.data.message;
                } else {
                    $scope.error = response.data.message;
                    $location.path('/ise');
                }
            });
        };
    }
);

app.controller('PostsCtrl', [
    '$scope',
    '$stateParams',
    'posts',
    '$rootScope',
    '$http',
    'auth',
    function($scope, $stateParams, posts, $rootScope, $http, auth) {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/posts/getall/' + $rootScope.userid,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.getToken()
            }
        }).then(function successCallback(response) {
            $scope.posts = response.data;
        }, function errorCallback(response) {
            console.log(response);
            // show error message
            if (response.data !== null) {
                $scope.error = response.data.message;
            } else {
                $scope.error = response.data.message;
                $location.path('/ise');
            }
        });

        $scope.upvote = function(post) {
            $http({
                method: 'PUT',
                url: 'http://localhost:3000/posts/upvote/' + $rootScope.userid + '/' + post._id,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                }
            }).then(function successCallback(response) {
                post.votes++;
            }, function errorCallback(response) {
                console.log(response);
                // show error message
                if (response.data !== null) {
                    $scope.error = response.data.message;
                } else {
                    $scope.error = response.data.message;
                    $location.path('/ise');
                }
            });
        };

        $scope.downvote = function(post) {
            $http({
                method: 'PUT',
                url: 'http://localhost:3000/posts/downvote/' + $rootScope.userid + '/' + post._id,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                }
            }).then(function successCallback(response) {
                post.votes--;
            }, function errorCallback(response) {
                console.log(response);
                // show error message
                if (response.data !== null) {
                    $scope.error = response.data.message;
                } else {
                    $scope.error = response.data.message;
                    $location.path('/ise');
                }
            });
        };
    }
]);

app.controller('SinglePostCtrl',
    function($scope, $stateParams, $rootScope, $http, auth) {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/posts/getone/' + $stateParams.id,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.getToken()
            }
        }).then(function successCallback(response) {
            $scope.post = response.data;
        }, function errorCallback(response) {
            console.log(response);
            // show error message
            if (response.data !== null) {
                $scope.error = response.data.message;
            } else {
                $scope.error = response.data.message;
            }
        });

        $http({
            method: 'GET',
            url: 'http://localhost:3000/posts/getcomments/' + $stateParams.id,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.getToken()
            }
        }).then(function successCallback(response) {
            $scope.post.comments = response.data;
        }, function errorCallback(response) {
            console.log(response);
            // show error message
            if (response.data !== null) {
                $scope.error = response.data.message;
            } else {
                $scope.error = response.data.message;
            }
        });

        $scope.addComment = function() {
            if ($scope.body === '') {
                return;
            }
            $http({
                method: 'POST',
                url: 'http://localhost:3000/posts/addcomment/' + $rootScope.userid + '/' + $scope.post._id,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                },
                data: {
                    body: $scope.body
                }
            }).then(function succesCallBack(response) {
                $scope.post.comments.push({
                    comment: response.data.comment,
                    authorname: response.data.authorname,
                    votes: response.data.votes,
                    _id: response.data._id
                });
            }, function errorCallback(response) {
                if (response.data !== null) {
                    $scope.error = response.data.message;
                }
            });

            $scope.body = '';
        };

        $scope.upvote = function(comment) {
            $http({
                method: 'PUT',
                url: 'http://localhost:3000/comments/upvote/' + $rootScope.userid + '/' + comment._id,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                }
            }).then(function succesCallBack(response) {
                if (!response.data) {
                    $scope.error = "Already voted!";
                } else
                    comment.votes++;
            }, function errorCallback(response) {
                if (response.data !== null) {
                    $scope.error = "Already voted!";
                }
            });
        }
    }
);

app.controller('AuthCtrl', [
    '$scope',
    '$state',
    'auth',
    function($scope, $state, auth) {
        $scope.user = {};

        $scope.register = function() {
            auth.register($scope.user).error(function(error) {
                $scope.error = error;
            }).then(function() {
                $state.go('home');
            });
        };

        $scope.logIn = function() {
            auth.logIn($scope.user).error(function(error) {
                $scope.error = error;
            }).then(function() {
                $state.go('home');
            });
        };
    }
]);

app.factory('auth', ['$http', '$window', function($http, $window) {
    var auth = {};
    auth.saveToken = function(token) {
        $window.localStorage['flapper-news-token'] = token;
    };

    auth.getToken = function() {
        return $window.localStorage['flapper-news-token'];
    };
    auth.isLoggedIn = function() {
        var token = auth.getToken();

        if (token) {
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload.exp > Date.now() / 1000;
        } else {
            return false;
        }
    };

    auth.currentUser = function() {
        if (auth.isLoggedIn()) {
            var token = auth.getToken();
            var payload = JSON.parse($window.atob(token.split('.')[1]));

            return payload.username;
        }
    };

    auth.logOut = function() {
        $window.localStorage.removeItem('flapper-news-token');
    };
    return auth;
}])

app.factory('posts', ['$http', function($http, $rootScope) {
    // service
    var o = {
        posts: []
    };

    return o;
}]);