var app = angular.module('flapperNews');

app.controller('PostsCtrl', function($scope, $stateParams, posts, $rootScope, $http, auth) {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/posts/getall/' + $rootScope.userid,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.getToken()
            }
        }).then(function successCallback(response) {
            $scope.posts = response.data;
        }, function errorCallback(response) {
            console.log(response);
            // show error message
            if (response.data !== null) {
                $scope.error = response.data.message;
            } else {
                $scope.error = response.data.message;
                $location.path('/ise');
            }
        });

        $scope.upvote = function(post) {
            $http({
                method: 'PUT',
                url: 'http://localhost:3000/posts/upvote/' + $rootScope.userid + '/' + post._id,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                }
            }).then(function successCallback(response) {
                post.votes++;
            }, function errorCallback(response) {
                console.log(response);
                // show error message
                if (response.data !== null) {
                    $scope.error = response.data.message;
                } else {
                    $scope.error = response.data.message;
                    $location.path('/ise');
                }
            });
        };

        $scope.downvote = function(post) {
            $http({
                method: 'PUT',
                url: 'http://localhost:3000/posts/downvote/' + $rootScope.userid + '/' + post._id,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                }
            }).then(function successCallback(response) {
                post.votes--;
            }, function errorCallback(response) {
                console.log(response);
                // show error message
                if (response.data !== null) {
                    $scope.error = response.data.message;
                } else {
                    $scope.error = response.data.message;
                    $location.path('/ise');
                }
            });
        };
    }
);