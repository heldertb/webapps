var app = angular.module('flapperNews');

app.controller('SinglePostCtrl',
    function($scope, $stateParams, $rootScope, $http, auth) {
        $http({
            method: 'GET',
            url: 'http://localhost:3000/posts/getone/' + $stateParams.id,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.getToken()
            }
        }).then(function successCallback(response) {
            console.log('post: ', response.data);
            $scope.post = response.data;
        }, function errorCallback(response) {
            console.log(response);
            // show error message
            if (response.data !== null) {
                $scope.error = response.data.message;
            } else {
                $scope.error = response.data.message;
            }
        });

        $http({
            method: 'GET',
            url: 'http://localhost:3000/posts/getcomments/' + $stateParams.id,
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + auth.getToken()
            }
        }).then(function successCallback(response) {
            console.log('comments: ', response.data);
            $scope.post.comments = response.data;
        }, function errorCallback(response) {
            console.log(response);
            // show error message
            if (response.data !== null) {
                $scope.error = response.data.message;
            } else {
                $scope.error = response.data.message;
            }
        });

        $scope.addComment = function() {
            if ($scope.body === '') {
                return;
            }
            $http({
                method: 'POST',
                url: 'http://localhost:3000/posts/addcomment/' + $rootScope.userid + '/' + $scope.post._id,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                },
                data: {
                    body: $scope.body
                }
            }).then(function succesCallBack(response) {
                $scope.post.comments.push({
                    comment: response.data.comment,
                    authorname: response.data.authorname,
                    votes: response.data.votes
                });
            }, function errorCallback(response) {
                console.log(response);
                if (response.data !== null) {
                    $scope.error = response.data.message;
                } 
            });

            $scope.body = '';
        };

        $scope.upvote = function(comment) {
            $http({
                method: 'PUT',
                url: 'http://localhost:3000/comments/upvote/' + $rootScope.userid + '/' + comment._id,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                }
            }).then(function succesCallBack(response) {
                comment.votes++;
            }, function errorCallback(response) {
                console.log(response);
                if (response.data !== null) {
                    $scope.error = response.data.message;
                }
            });
        }
    }
);