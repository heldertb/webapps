var app = angular.module('flapperNews');

app.controller('MainCtrl', function($scope, $location, $rootScope, $http, auth) {

    $scope.login = function() {
        if ($scope.username !== null && $scope.password !== null) {

            $http({
                method: 'POST',
                url: 'http://localhost:3000/login',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json'
                },
                data: {
                    username: $scope.username,
                    password: $scope.password
                }
            }).then(function successCallback(response) {
                $rootScope.loggedIn = true;
                $rootScope.username = $scope.username;
                $rootScope.userid = response.data.id;
                auth.saveToken(response.data.token);
                $location.path('/home');
            }, function errorCallback(response) {
                console.log(response);
                // show error message
                if (response.data !== null) {
                    $scope.error = response.data.message;
                } else {
                    $scope.error = response.data.message;
                    $location.path('/ise');
                }
            });
        } else {
            $scope.error = "Gelieve alle gegevens in te vullen.";
        }
    };

    $scope.registerredirect = function() {
        $location.path('/register');
    };

    $scope.postRedirect = function() {
        $location.path('/addpost');
    }

    $scope.register = function() {
        $rootScope.username = '';
        console.log('un', $scope.username,
            'pw', $scope.password,
            'fn', $scope.firstName,
            'ln', $scope.lastName,
            'em', $scope.email,
            'g', $scope.gender);
        if ($scope.password === $scope.confirmpassword) {
            if ($scope.email !== null && $scope.password !== null && $scope.confirmpassword !== null && $scope.firstName !== null && $scope.lastName !== null &&
                $scope.email !== '' && $scope.password !== '' && $scope.confirmpassword !== '' && $scope.firstName !== '' && $scope.lastName !== '') {
                if ($scope.username === '' || $scope.username === null) {
                    $scope.username = $scope.email;
                }
                console.log($scope.username);
                $http({
                    method: 'POST',
                    url: 'http://localhost:3000/register',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json'
                    },
                    data: {
                        username: $scope.username,
                        password: $scope.password,
                        firstname: $scope.firstName,
                        lastname: $scope.lastName,
                        email: $scope.email,
                        gender: $scope.gender
                    }
                }).then(function succesCallBack(response) {
                    console.log(response.id);
                    $rootScope.loggedIn = true;
                    $rootScope.username = $scope.username;
                    $rootScope.userid = response.data.id;
                    auth.saveToken(response.data.token);
                    $location.path('/home');
                }, function errorCallback(response) {
                    console.log(response);
                    if (response.data !== null) {
                        $scope.error = response.data.message;
                    } else {
                        $location.path('/ise');
                    }
                });
            } else {
                $scope.error = "Please fill in all required fields.";
            }
        } else {
            $scope.error = "Password and confirm password have to be te same.";
        }
    };
});