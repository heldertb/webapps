var app = angular.module('flapperNews');

app.controller('AddPostCtrl',
    function($scope, $location, $rootScope, $http, auth) {
        $scope.addPost = function() {
            if (!$scope.title || $scope.title === '') {
                return;
            }
            $http({
                method: 'POST',
                url: 'http://localhost:3000/posts/add/' + $rootScope.userid,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + auth.getToken()
                },
                data: {
                    title: $scope.title,
                    body: $scope.body,
                    link: $scope.link
                }
            }).then(function successCallback(response) {
                $location.path('/home');
            }, function errorCallback(response) {
                console.log(response);
                // show error message
                if (response.data !== null) {
                    $scope.error = response.data.message;
                } else {
                    $scope.error = response.data.message;
                    $location.path('/ise');
                }
            });
        };
    }
);